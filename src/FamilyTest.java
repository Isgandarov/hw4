public class FamilyTest {
    public static void main(String[] args) {
        Human per[] = new Human[10];
        Pet pt[] = new Pet[10];

        pt[0] = new Pet("dog", "Shepherd", new String[]{"barking", "sleeping"}, 5, 24);
        per[0] = new Human("Jax", "Teller", pt[0].getSpecie(), "Gemma", "John", 120, 1990);
        pt[1] = new Pet("cat", "Violet", new String[]{"eating", "disturbing"}, 3, 50);
        per[1] = new Human("Bob", "Harley", pt[0].getSpecie(), "Jasie", "Mike", 100, 1997);


        String petInfos = pt[0].toString();
        System.out.println(pt[0].toString());
        System.out.println(per[0].toString(petInfos));

        pt[1].respond();
        pt[1].eat();
        pt[0].foul();

        per[0].describePet(pt[0].getSpecie(), pt[0].getAge());
        per[1].feedPet(pt[1].getTrickLevel(), per[1].getName(), pt[1].getSpecie());


    }
}
